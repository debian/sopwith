This is intended as a guiding document for the project.

### True to the original

* Sopwith has a long history that deserves to be honored and preserved. By
  default, the game should always play like the original DOS version. That
  means the gameplay in particular should be the same, without any significant
  differences. Someone who has just discovered the project should find it to
  be a delightfully accurate recreation of the game they may have played when
  they were younger.

* Bugs in the original code can be fixed if they don't make significantly
  noticeable or controversial changes the original gameplay. An example is
  that in the original version it was possible to flip the plane upside down
  while it was sitting motionless on the runway.

* Some new features can be enabled by default, as long as they are subtle,
  unintrusive, carefully considered and can be turned off. An example is the
  medals feature.

* The game will never try to be "something it's not". This means that it will
  always have four color CGA graphics, PC speaker sound effects and a low
  resolution display. It will never add (for example) hi-res sprites or 3D
  models, digital sound effects or MP3 music. The goal is to be "a great old
  game" rather than "a mediocre modern game".

### New features

* New features should be fun and recognize the comical aspects of the game.
  Features should be carefully considered before being incorporated, not just
  added arbitrarily and thoughtlessly.

* Custom levels are the perfect opportunity to introduce new features without
  changing the original game. Some hypothetical examples might be:

  - New levels might feature new items that weren't present in the original
    level, like artillery or observation balloons.
  - In the original level, the enemy planes all have separate territories.
    Perhaps in a new level, enemy planes that share a territory know to fly in
    formation.

* Multiplayer is also a perfect opportunity for new features and gameplay
  expansions, since the multiplayer functionality in the original game only
  worked with the proprietary BMB network hardware and few people ever got to
  experience it.

### The setting

* Sopwith has a World War I theme. Any new features should keep to this
  theme. Jet planes and cruise missiles would be anachronistic and not
  appropriate. A feature from the Author's Edition that previously added
  missiles and flares, for example, was removed. However, the primary aim of
  the game is to be fun rather than realistic.

* It should be remembered that World War I was a real world conflict in which
  around 40 million people died, and that should be respected. The game will
  never depict people, only vehicles, buildings and other inanimate objects.

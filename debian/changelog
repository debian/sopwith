sopwith (2.7.0-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 12 Nov 2024 17:41:53 +0000

sopwith (2.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards version to 4.7.0.0 (no packaging changes).
  * Adjust debian/not-installed and debian/sopwith.manpages for new files.
  * Adjust debian/copyright to reflect license for new metadata file.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 29 Oct 2024 16:12:14 +0000

sopwith (2.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 22 Apr 2024 17:50:59 +0000

sopwith (2.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/not-installed to address warning from dh_missing.
  * Change debian/control build-dep from obsolete pkg-config to pkgconf.
  * Update copyright date to 2024 on Debian maintainer files.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 10 Feb 2024 17:25:49 +0000

sopwith (2.3.0-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 30 May 2023 00:06:11 +0000

sopwith (2.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove unused entries from debian/copyright (due to upstream changes).
  * Bump standards version to 4.6.2.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 18 May 2023 02:05:10 +0000

sopwith (2.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 06 Nov 2022 14:20:52 +0000

sopwith (2.1.0-3) unstable; urgency=medium

  * Adjust debian/watch configuration (old config somehow stopped working).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 17 Sep 2022 01:42:57 +0000

sopwith (2.1.0-2) unstable; urgency=medium

  * Accept changes from Debian Janitor
     * Set upstream metadata fields: Bug-Submit, Repository.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 07 Sep 2022 02:28:54 +0000

sopwith (2.1.0-1) unstable; urgency=medium

  * Package major upstream v2.0.0 release (closes: #1017755).
    - New release upgrades to SDL 2 (closes: #894853)
    - Update README files, copyright, etc. to reflect current state
    - Update debian/* files to reflect new code structure and GitHub location
    - Remove patches, which are all outdated for v2.0.0

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 02 Sep 2022 00:06:07 +0000

sopwith (1.8.4-18) unstable; urgency=medium

  * Adjust lintian overrides.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 27 Jan 2022 02:08:52 +0000

sopwith (1.8.4-17) unstable; urgency=medium

  * Accept package cleanup from Debian Janitor.
    - Remove constraints unnecessary since buster
    - Build-Depends: Drop versioned constraint on dpkg-dev and pkg-config

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 24 Sep 2021 19:10:05 +0000

sopwith (1.8.4-16) unstable; urgency=medium

  * Bump standards version to 4.6.0.0 (no packaging changes).
  * Update debian/watch to reflect version 4 format (no changes).
  * Remove unneeded Lintian source override configure-generated-file-in-source.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 18 Aug 2021 16:11:49 +0000

sopwith (1.8.4-15) unstable; urgency=medium

  * Package cleanup based on a review at qa.debian.org.
    - Remove sopwith.menu per Lintian command-in-menu-file-and-desktop-file
    - Remove obsolete Lintian override for testsuite-autopkgtest-missing
    - Add Lintian override for debian-watch-does-not-check-gpg-signature
    - Add Lintian override for maintainer-desktop-entry
    - Add Lintian override for deprecated-configure-filename
    - Add Lintian override for configure-generated-file-in-source (it's fixed)
    - Change Lintian override to "Fowarded: not-needed" for headers.patch
    - Update debian/control to move to debhelper-compat (= 13)
    - Update debian/control to set Rules-Requires-Root: no
    - Add a debian/upstream/metadata file

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 29 Oct 2020 20:29:53 +0000

sopwith (1.8.4-14) unstable; urgency=medium

  * Remove build dependency on unused, deprecated GTK2 (closes: #967752).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 06 Aug 2020 22:05:11 +0000

sopwith (1.8.4-13) unstable; urgency=low

  * Ignore new lintian warning about submitting patch upstream.
  * Accept package cleanup from Debian Janitor.
    - Trim trailing whitespace
    - Move source package lintian overrides to debian/source

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 16 Jul 2020 23:05:34 +0000

sopwith (1.8.4-11) unstable; urgency=medium

  * Disable broken and undocumented ~/.sopwithrc to allow game to run.
  * Bump standards version to 4.5.0.0 (no packaging changes).
  * Make adjustments to account for new packaging behavior.
    - Remove unneeded entries from debian/source/include-binaries
    - Add files to debian/not-installed to quiet dh-missing

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 27 Jun 2020 18:10:21 +0000

sopwith (1.8.4-10) unstable; urgency=medium

  * Bump standards version to 4.4.0.1 (no packaging changes).
  * Move to debhelper compatibility level 12.
    - Update debian/control to build depend on debhelper-compat (= 12)
    - Remove debian/compat, which is no longer needed
    - Adjust debian/rules to invoke dh using --without autoreconf
    - Manually update configure to support --runstatedir
    - Remove Build-Depends on autotools-dev, per lintian warning

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 10 Aug 2019 19:39:14 +0000

sopwith (1.8.4-9) unstable; urgency=medium

  * Use https protocol in debian/watch (contributed by Ondřej Nový).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 20 Oct 2018 10:39:39 +0000

sopwith (1.8.4-8) unstable; urgency=medium

  * Bump standards version to 4.2.1.0 (no packaging changes).
  * Convert to correct https copyright file URI.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 16 Sep 2018 18:22:25 +0000

sopwith (1.8.4-7) unstable; urgency=medium

  * Bump standards version to 4.2.0.1 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 07 Aug 2018 01:52:44 +0000

sopwith (1.8.4-6) unstable; urgency=medium

  * Convert Debian packaging from Subversion to Git.
  * Bump standards version to 4.1.3.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 03 Jan 2018 02:43:59 +0000

sopwith (1.8.4-5) unstable; urgency=medium

  * Bump standards version to 4.1.1.0 (no packaging changes).
  * Fix Lintian warnings.
    - Remove deprecated "--with autotools_dev" from Debian rules
    - Use DEB_CFLAGS_MAINT_APPEND=-O0, not DEB_BUILD_OPTIONS=noopt

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 12 Nov 2017 21:18:48 +0000

sopwith (1.8.4-4) unstable; urgency=medium

  * Ignore the Lintian testsuite-autopkgtest-missing warning.
  * Bump standards version to 4.0.0.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 02 Jul 2017 21:04:45 +0000

sopwith (1.8.4-3) unstable; urgency=medium

  * Bump standards version to 3.9.8.0 (no packaging changes).
  * Export "DEB_BUILD_MAINT_OPTIONS = hardening=+all" in debian/rules.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 20 Jun 2016 20:55:10 +0000

sopwith (1.8.4-2) unstable; urgency=medium

  * Bump standards version to 3.9.6.1 (no packaging changes).
  * Fix all patches to eliminate the unnecessary leading # character.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 05 Aug 2015 13:55:48 -0500

sopwith (1.8.4-1) unstable; urgency=medium

  * New upstream release.
  * Clean up patches.
    - Add remove-auto-files.patch to remove leftover config.status
    - Remove debian/patches/manpage.patch, since changes are now in upstream

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 22 Nov 2014 16:38:21 +0000

sopwith (1.8.3-2) unstable; urgency=medium

  * Fix Lintian bug in debian/copyright: empty-short-license-in-dep5-copyright.
  * Bump standards version to 3.9.6.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 03 Oct 2014 20:05:40 +0000

sopwith (1.8.3-1) unstable; urgency=medium

  * New upstream release.
    - Upstream changes fix potential compile problem (closes: #748172).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 26 May 2014 14:21:51 +0000

sopwith (1.8.2+clean-1) unstable; urgency=medium

  * Release with a modified tarball to remove accidentally-included .o files.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 29 Apr 2014 19:49:30 +0000

sopwith (1.8.2-1) unstable; urgency=medium

  * New upstream release.
  * Fix segmentation fault in single player mode (closes: #742295).
    - Add 'export DEB_BUILD_OPTIONS=noopt' in debian/rules to build with -O0
    - Add lintian overrides for hardening-no-fortify-functions

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 22 Mar 2014 16:51:57 +0000

sopwith (1.8.1-4) unstable; urgency=low

  * Bump standards version to 3.9.5.0 (no packaging changes).
  * Fix Lintian warning about missing Keyword key in debian/sopwith.desktop.
  * Improve Debian menu definition (closes: #738031).
    - Reformat debian/sopwith.menu to put all fields on separate lines
    - Add icon field in debian/sopwith.menu, per suggestion from submitter
    - Add longtitle field in debian/sopwith menu, to be more standard
    - Create new XPM icon debian/sopwith.xpm based on sopwith.png
    - Update debian/sopwith.install to put sopwith.xpm in /usr/share/pixmaps

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 23 Feb 2014 15:53:08 +0000

sopwith (1.8.1-3) unstable; urgency=low

  * Update debian/rules to touch config.status in clean rule, to fix
    FTBFS from 'make clean' if the file doesn't exist (closes: #718128).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 28 Jul 2013 23:01:52 +0000

sopwith (1.8.1-2) unstable; urgency=low

  * Convert debian/rules to debhelper7, overriding just a few standard targets.
  * Update debian/sopwith.install to handle desktop items with a relative path.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 24 Apr 2013 16:38:52 +0000

sopwith (1.8.1-1) unstable; urgency=low

  * New upstream release.
    - Update debian/patches/remove-auto-files.patch to match upstream tarball
    - Update debian/patches/manpage.patch to work with latest upstream file

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 27 Mar 2013 14:45:07 +0000

sopwith (1.8.0-1) unstable; urgency=low

  * New upstream release.
    - Update debian/patches/remove-auto-files.patch to match new code

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 01 Mar 2013 00:49:47 +0000

sopwith (1.7.5-2) unstable; urgency=low

  * Bump standards version to 3.9.4.0; no packaging changes.
  * Fix buildd log scanner W-build-stamp-in-binary warning in debian/rules.
    - Get rid of configure target (causes problems because ./configure exists)
  * Make other minor tweaks in debian/rules for consistency.
    - Add new .PHONY targets for all rules that don't create files
    - Change binary to depend on both binary-arch and binary-indep
    - Change binary-arch to depend on install, not build and install

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 31 Dec 2012 18:43:28 +0000

sopwith (1.7.5-1) unstable; urgency=low

  * New upstream release.
    - Update debian/patches/remove-auto-files.patch to match new code
  * Fix typo in package description (closes: #672796), patch by Jon Dowland.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 19 Sep 2012 22:17:57 -0500

sopwith (1.7.4-6) unstable; urgency=low

  * Fix FTBFS by rearranging debian/rules slightly (closes: #666354).
    - Move configure from build to build-stamp, so it's run consistently.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 31 Mar 2012 21:46:01 +0000

sopwith (1.7.4-5) unstable; urgency=low

  * Update to machine-readable debian/copyright file format, version 1.0.
  * Bump standards version to 3.9.3.0; no packaging changes.
  * Support compile-time hardening to meet the release goal for wheezy.
    - Move to debhelper (>= 9) and debian/compat=9
    - Use dpkg-buildflags rather than importing buildflags.mk in debian/rules
    - Add Build-Depends on dpkg-dev (>= 1.16.1), for dpkg-buildflags

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 12 Mar 2012 12:06:41 -0500

sopwith (1.7.4-4) unstable; urgency=low

  * Change debian/control to remove unneeded Suggests: menu (closes: #647378).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 03 Nov 2011 20:16:50 -0500

sopwith (1.7.4-3) unstable; urgency=low

  * Fix typo in package description.
  * Tweak debian/rules to clean up config.log and config.status after install.
  * Handle changes to build flag behavior introduced with dpkg-dev 1.16.1.
    - Modify debian/rules to set DPKG_EXPORT_BUILDFLAGS=1 and call buildflags.mk

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 26 Sep 2011 12:08:02 -0500

sopwith (1.7.4-2) unstable; urgency=low

  * Bump standards version to 3.9.2.0; no packaging changes.
  * Fix Lintian warning: debian-rules-missing-recommended-target
    - Restructure build, build-arch, build-indep targets in debian/rules

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 05 Aug 2011 15:58:12 -0500

sopwith (1.7.4-1) unstable; urgency=low

  * New upstream release.
    - Upstream release improves oil tank explosions (closes: #188298)
  * Clean up patches.
    - Upstream now includes spelling-errors.patch, so remove it
    - Upstream now includes part of headers.patch, so tweak it
    - Add remove-auto-files.patch to remove config.log and config.status
  * Pass --program-prefix="" to ./configure so binary installs properly.
  * Bump standards version to 3.9.1.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 13 Sep 2010 20:47:20 -0500

sopwith (1.7.3-4) unstable; urgency=low

  * Fix warnings from the Full Lintian Report in the PTS.
    - Properly escape "-" in sopwith.6 (in manpage.patch)
    - Fix a spelling error in swasynio.c (in spelling-error.patch)

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 11 Jul 2010 02:25:02 +0000

sopwith (1.7.3-3) unstable; urgency=low

  * List Jesse Smith as the upstream maintainer in debian/copyright.
  * Updates to Debian manpage, which will be pushed upstream.
    - Correct the keys in the manpage to match doc/keys.txt
    - Document the new -g command-line option to choose game level

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 06 Jul 2010 10:40:00 -0500

sopwith (1.7.3-2) unstable; urgency=low

  * New maintainer (closes: #472738).
  * Bump standards version to 3.9.0.0.
  * Tweak long description to make it more standard (no "I present...").
  * Convert debian/copyright to new DEP-5 machine-readable format.
  * Create patches headers.patch and manpage.patch out of auto-generated patch.
  * Update manpage and README.Debian now that audio is disabled by default.
  * Changes to build process in debian/rules and debian/control:
    - Bump debhelper compatibility to 7, per linitian recommendation
    - Remove -Wall compiler option: upstream doesn't use it, and it adds noise
    - Take $(CFLAGS) as passed to debian/rules and provide to ./configure
    - Use dh_prep instead of dh_clean -k, per deprecation warning
    - Install sopwith.png rather than uuencoding, since 3.0 (quilt) allows it
    - Remove Build-Depends on sharutils, since uudecode is no longer used
    - Add new Build-Depends on autotools-dev, as source for config.sub|guess
    - Standardize autotools behavior per autotools-dev instructions
    - Rework installation behavior via dh_install/man/doc

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 02 Jul 2010 21:27:15 -0500

sopwith (1.7.3-1) unstable; urgency=low

  * QA upload
  * New upstream release (closes: #585929). This release fixes the
    following upstream bugs:
    - Range check and missile firing bug fixed (closes: #224518, #258751)
    - Mouse pointer no longer appears over the game window (closes: #187930)
    - Throttle now goes up and down smoothly (closes: #188391)
    - Cleaned up compiler warnings and errors (closes: #582399)
    Thanks to Jesse Smith for compiling this list of closed upstream bugs.
  * Fixed debian/watch, use new name of upstream tarball.
  * DH-compat level 5, bumped versioned dependency on debhelper.
  * Switch to source format 3.0 (quilt).
  * debian/rules: removed invocation of deprecated dh_desktop.
  * debian/control: added dependency on ${misc:Depends}.
  * debian/copyright: symlink to versionend GPL file.

 -- Ralf Treinen <treinen@debian.org>  Sat, 19 Jun 2010 22:44:08 +0200

sopwith (1.7.1-4) unstable; urgency=low

  * QA upload
    + Change maintainer to QA group
  * Incorporate Ubuntu patch, adds .desktop file and
    icon. Patch by Leslie Viljoen with an improvement
    by Daniel Dickinson. (Closes: #478935, #479989)
  * Move homepage from Description to dedicated field
  * Add watch file
  * Remove unneeded dpatch dependency
  * Add the actual copyright statements in debian/copyright
  * Adapt menu file to current menu policy
  * Add some missing includes

 -- Frank Lichtenheld <djpig@debian.org>  Thu, 17 Jul 2008 22:53:56 +0200

sopwith (1.7.1-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Applied patch from Jon Dowland to fix build failure using dash as
    /bin/sh (Closes: #378200)

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 15 Jan 2008 21:33:57 +0100

sopwith (1.7.1-3) unstable; urgency=low

  * Apply patch from Andreas Jochens <aj@andaco.de> (thanks) to fix FTBFS on
    AMD64 (Closes: #298413)
  * Quote bits in debian/menu, thanks linda and lintian.
  * Update FSF address in debian/copyright, also thanks linda and lintian.
  * Update to standards-version 3.6.2.0, no changes required.
  * Change Maintainer: email address from my private address to
    dbharris@debian.org

 -- David B. Harris <dbharris@debian.org>  Sun, 19 Feb 2006 12:15:55 -0500

sopwith (1.7.1-1) unstable; urgency=medium

  * New upstream release (Closes: #206251)
    + Upstream now allows the use of Left/Right arrow keys to control the
      plane, in addition to the traditional / and , (Closes: #186898)
  * debian/patches/00list:
    + Remove 90_ctrlkeyfix, which has been integrated upstream
  * debian/control:
    + Bump Standards-Version to 3.6.1.0, no changes required
    + Add pkg-config and libgtk2.0-dev to Build-Depends, as upstream requires

 -- David B Harris <david@eelf.ddts.net>  Sun, 31 Aug 2003 04:28:46 -0400

sopwith (1.6.0-3) unstable; urgency=low

  * debian/rules: ensure $(CFLAGS) gets passed to ./configure
  * Convert to dpatch to modify upstream source:
    + debian/patches/90_ctrlkeyfix.dpatch:
      - Allow both left and right control keys to be used in CTRL+C for
        quitting (previously only left was allowed). Thanks to Tollef Fog Heen
        for pointing this out.
      - Document using CTRL+C to quit in sopwith.6 manpage (Closes: #185449)
    + debian/patches/00list: Add 90_ctrlkeyfix
  * debian/menu: Execute /usr/games/sopwith, not /usr/bin/sopwith
  * debian/control: Add Suggests: menu (>> 1.5)

 -- David B Harris <david@eelf.ddts.net>  Tue, 18 Mar 2003 03:17:31 -0500

sopwith (1.6.0-2) unstable; urgency=low

  * Fixed punctuation inconsistency in Description.
  * Start using dh_install.
  * Put the sopwith binary in /usr/games, where it belongs.

 -- David B Harris <david@eelf.ddts.net>  Thu, 13 Mar 2003 09:54:35 -0500

sopwith (1.6.0-1) unstable; urgency=low

  * Initial Release. (Closes: #150759)

 -- David B Harris <david@eelf.ddts.net>  Tue,  4 Mar 2003 23:57:25 -0500
